window.onload = function(){
        console.log('16/03/2021 es una fecha valida? '+validarFecha('16/03/2021'));
        console.log('100/03/2021 es una fecha valida? '+validarFecha('100/03/2021'));
        console.log('daarkAssassin@gmail.com es un email valido? '+validarMail('daarkAssassin@gmail.com'));
        console.log('lorem@.co es un email valido? '+validarMail('lorem@.co'));
        console.log(validarReemplazo('&&&&\"\"<<<<<>>>>>'));
        console.log(eliminarHtml('<html>hola mundo <script> var hola </script> </html>'));
        console.log(invertirNombreApellido("David Bonet"));

    function validarFecha(fechaString){
        var date_regEx = /^\d{2}[./-]\d{2}[./-]\d{4}$/
        if(!fechaString.match(date_regEx)) return false;  // Formato no valido
        return true;
    }
    function validarMail(mailString){
        var mail_regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!mailString.match(mail_regEx)) return false;  // Formato no valido
        return true;

    }
    function validarReemplazo(text){
        text = text.replace(/[&]/g, '&amp;');
        text = text.replace(/[\"]/g, '&quot;');
        text = text.replace(/[<]/g, '&lt;');
        text = text.replace(/[>]/g, '&gt;');
        return text;
    }
    function invertirNombreApellido(text){
        var nam_regEx = /^[a-z ,.'-]+$/i;
        return text.replace(/^(.+?) ([^\s,]+)(,? )?$/i,"$2, $1$3");


    }
    function eliminarHtml(text){
        var html_regEx = /<script[^>]*?>[\s\S]*?<\/script>/gi;
        text = text.replace(html_regEx, '');
        return text;
    }

}
