window.onload = function(){ //Acciones tras cargar la página
    pantalla=document.getElementById("resultado"); //elemento pantalla de salida
}

x="0"; //número en pantalla
y="0";
op="no"; //operacion en curso, si 'no' significa que no hay operacion en curso.
coma = "0"; //0 = no hay coma, 1 si hay coma.
    
function numero(xx) { 
    if (x=="0") {
        pantalla.innerHTML=xx; 
        x=xx; 
        if (xx==".") { 
            pantalla.innerHTML="0.";
            x=xx;
        }
    } else { 
        if (xx=="." && coma==0) {
            pantalla.innerHTML+=xx;
            x+=xx;
            coma=1;
        }else if (xx=="." && coma==1) {
            //no ocurre nada.
        }else {
            pantalla.innerHTML+=xx;
            x+=xx
        }
    }
}

function operar(s) {
    igualar();
    y=x;
    x=0;
    op=s;
}	

function igualar() {
    if (op=="no") {
        //no ocurre nada.
    }else { 
        operacion = y+op+x;
        solucion = eval(operacion);
        pantalla.innerHTML = solucion;
        x = solucion;
        op = "no";
    }
}
function raizc() {
    x = Math.sqrt(x);
    pantalla.innerHTML=x;
    op="no";
}

function porcent() { 
    x = x/100;
    pantalla.innerHTML=x;
    igualar();
}

function opuest() { 
    var nx = Number(x);
    nx=-nx;
    x=String(nx);
    pantalla.innerHTML=x;
}

function inve() {
    var nx=Number(x);
    nx = (1/nx);
    x = String(nx);		 
    pantalla.innerHTML=x;
}

function delTotal() {
    pantalla.innerHTML=0; 
    x="0"; 
    y="0";
    coma=0;
    op="no" 
}