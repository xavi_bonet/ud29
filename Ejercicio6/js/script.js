window.onload = function(){
    reloj();
    function reloj(){
        fechaActual = new Date();
        hora = fechaActual.getHours();
        minuto = fechaActual.getMinutes();
        segundo = fechaActual.getSeconds();
    
        document.getElementById('reloj').value = hora + " : " + minuto + " : " + segundo;

        setTimeout(function(){ reloj() }, 1000);

    }

}
